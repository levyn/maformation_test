const CsvToSqlLite = require("./converter");
const Web = require("./webSrv");
const sqlite3Promise = require("./utils/promiseSqlLite");

// Start with script args
main(process.argv.slice(2));

function main(args) {
  switch (args[0]) {
    case "converter":
      CsvToSqlLite(args[1], args[2]);
      break;
    case "webclient":
      Web.start(args[1]);
      break;
    default:
      console.log(
        "npm start converter [csvInput] [dbOutput]\nnpm start webclient [dbName]"
      );
      process.exit();
  }
}

// properly handle node close event to close DB
// source : https://stackoverflow.com/questions/10021373/what-is-the-windows-equivalent-of-process-onsigint-in-node-js
if (process.platform === "win32") {
  var rl = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
  });

  rl.on("SIGINT", function() {
    process.emit("SIGINT");
  });
}

process.on("SIGINT", async function() {
  await sqlite3Promise.close();
  console.log("WebClient closed");
  process.exit();
});
