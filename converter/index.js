const csv = require("csv-parser");
const fs = require("fs");
const sqlite3Promise = require("../utils/promiseSqlLite");

function CsvToSqlLite(csvInput, sqlOutput) {
  if (!(csvInput && sqlOutput)) {
    console.log(
      "Incorrect args, converter usage : \n npm start converter [csvInput] [dbOutput]"
    );
    process.exit(0);
  }

  const csvFile = csvInput.split(".")[0] + ".csv";
  const sqlFile = sqlOutput.split(".")[0] + ".db";

  sqlite3Promise
    .open("./db/" + sqlFile)
    .then(res => {
      readCsv(csvFile).then(csvDatas => {
        insertToDB(csvDatas);
      });
    })
    .catch(err => console.log(err));
}

//read csv content from file path
function readCsv(file) {
  return new Promise((resolve, reject) => {
    const results = [];
    fs.createReadStream(file)
      .pipe(csv({ separator: ";" }))
      .on("data", data => results.push(data))
      .on("end", async () => {
        resolve(results);
      });
  });
}

//insert CSV content into the DB based on it's rows titles
async function insertToDB(datas) {
  const rowKeys = Object.keys(datas[0]).toString();
  const paramsIntero = rowKeys.replace(/\b(\w*[a-z]\w*)\b/g, "?");
  await sqlite3Promise.run(`CREATE TABLE IF NOT EXISTS users(${rowKeys})`);
  Promise.all(
    datas.map(async row => {
      const rawData = Object.values(row);
      return sqlite3Promise.run(
        `INSERT INTO users(${rowKeys}) VALUES(${paramsIntero})`,
        rawData
      );
    })
  ).then(res => {
    console.log(`${res.length} élément(s) enregistrés avec succès`);
    sqlite3Promise.close();
    process.exit();
  });
}
module.exports = CsvToSqlLite;
