const sqlite3Promise = require("../utils/promiseSqlLite");

function search(query) {
  return new Promise(async (resolve, reject) => {
    const searchText = query.search.value.trim(),
      start = parseInt(query.start),
      length = parseInt(query.length),
      order = query.order[0];
    try {
      //count total records
      const recordsTotal = await sqlite3Promise.get(
        "select count(*) as nb from users"
      );
      const findRes = await sqlite3Promise.all(prepareQuery(order), [
        searchText
      ]);
      //send only asked result
      resolve({
        draw: query.draw,
        recordsTotal: recordsTotal.nb,
        recordsFiltered: findRes.length,
        data: findRes
          .slice(start, start + length)
          .map(data => Object.values(data))
      });
    } catch (error) {
      reject(error);
    }
  });
}

//generate sql Query
function prepareQuery(order) {
  const base = `
  select rs,CASE typvoie WHEN '' THEN '' ELSE (typvoie || ' ') END  || voie as voiefull,cp , commune, telephone from users 
  where 
          nofinesset like ?1 or 
          nofinessej like ?1 or 
          rs like ?1 or 
          rslongue like ?1 or 
          complrs like ?1 or 
          numvoie like ?1 or 
          compvoie like ?1 or
          typvoie like ?1 or 
          voie like ?1 or 
          lieuditbp like ?1 or
          departement like ?1 or
          libdepartement like ?1 or 
          cp like ?1 or
          commune like ?1 or 
          telephone like ?1 or
          telecopie like ?1 or 
          dateouv like ?1 or 
          dateautor like ?1 or 
          datemaj like ?1 or
          wgs84 like ?1 or
          lat like ?1 or 
          lng like ?1
          order by ${parseInt(order.column) + 1} ${
    order.dir === "asc" ? "desc" : "asc"
  }`;
  return base;
}
module.exports = search;
