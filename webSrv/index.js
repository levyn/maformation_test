const express = require("express");
const app = express();
var search = require("./search");
const sqlite3Promise = require("../utils/promiseSqlLite");

const fs = require("fs");

app.use("/", express.static("public"));

exports.start = async function(dbname = "") {
  const dbFile = dbname.split(".")[0] + ".db";
  try {
    if (fs.existsSync(`./db/${dbFile}`)) {
      await sqlite3Promise.open(`./db/${dbFile}`);
      app.get("/api/search", function(req, res) {
        search(req.query)
          .then(searchRes => {
            res.json(searchRes);
          })
          .catch(err => {
            console.log(err);
            res.json({});
          });
      });
      app.listen(80);
      console.log("Open on port 80");
    } else {
      throw "db not exist";
    }
  } catch (err) {
    console.log(
      `Can't connect to ${dbFile}\nnpm start webclient [dbName]\n`,
      err
    );
    process.exit();
  }
};
