## Installation

    $ git clone https://gitlab.com/levyn/maformation_test
    $ cd PROJECT_TITLE
    $ npm install

## Utilisation

    $ npm start converter [csvInput] [dbOutputName]
    $ npm start webclient [dbName]
